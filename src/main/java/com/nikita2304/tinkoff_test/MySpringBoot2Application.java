package com.nikita2304.tinkoff_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySpringBoot2Application {

	public static void main(String[] args) {
		SpringApplication.run(MySpringBoot2Application.class, args);
	}
}
