package com.nikita2304.tinkoff_test.controller;

import com.nikita2304.tinkoff_test.exceptions.ContactNotFoundException;
import com.nikita2304.tinkoff_test.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;

@RestController
public class ContactController {

    @Autowired
    ContactRepository contactRepository;

    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    public ResponseEntity getAllContacts() {
        return new ResponseEntity(contactRepository.getAllContacts(), HttpStatus.OK);
    }

    @RequestMapping(value = "/contacts/{contactId}", method = RequestMethod.GET)
    public ResponseEntity getContact(@PathVariable("contactId") BigInteger contactId) throws ContactNotFoundException {
        return new ResponseEntity(contactRepository.getContact(contactId), HttpStatus.OK);
    }

}
