package com.nikita2304.tinkoff_test.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class GeneralController {


    @RequestMapping(value = "/")
    public String index() {
        return "This is test application!";
    }

}
