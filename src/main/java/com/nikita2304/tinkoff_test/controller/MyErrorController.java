package com.nikita2304.tinkoff_test.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MyErrorController implements ErrorController {

    String details = "Current application supports following requests:\n" +
            "\"/requests\" - show all requests in db.\n" +
            "\"/requests/requests-by-contact/{contactId}\" - show latest request for contactId in db.\n" +
            "\"/contacts\" - show all contacts in db.\n" +
            "\"/contacts/{contactId}\" - show contactId in db.\n";

    @RequestMapping("/error")
    @ResponseBody
    public String handleError(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        return String.format("<html><body><h2>Error Page</h2><div>Status code: <b>%s</b></div>"
                        + "<div>Exception Message: <b>%s</b></div><body></html>",
                statusCode, details);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
