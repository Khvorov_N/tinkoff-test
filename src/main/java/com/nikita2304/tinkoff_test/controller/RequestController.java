package com.nikita2304.tinkoff_test.controller;

import com.nikita2304.tinkoff_test.exceptions.ApplicationRequestNotFoundException;
import com.nikita2304.tinkoff_test.repository.ApplicationRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;

@RestController
public class RequestController {

    @Autowired
    ApplicationRequestRepository requestRepository;

    @RequestMapping(value = "/requests", method = RequestMethod.GET)
    public ResponseEntity getAllRequests() {
        return new ResponseEntity(requestRepository.getAllRequests(), HttpStatus.OK);
    }

    @RequestMapping(value = "/requests/requests-by-contact/{contactId}", method = RequestMethod.GET)
    public ResponseEntity getLatestRequestByCustomerId(@PathVariable("contactId") String contactId) throws ApplicationRequestNotFoundException {
        return new ResponseEntity(requestRepository.getLatestRequestByCustomerId(new BigInteger(contactId)), HttpStatus.OK);
    }
}
