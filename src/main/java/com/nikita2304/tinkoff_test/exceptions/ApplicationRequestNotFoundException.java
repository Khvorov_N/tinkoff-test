package com.nikita2304.tinkoff_test.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.math.BigInteger;

@ResponseStatus(HttpStatus.NOT_FOUND)

public class ApplicationRequestNotFoundException extends Exception {
    private static final String messagePart1 = "There is no requests in database for contact_id = ";
    private static final String messagePart2 = ". Check if customer exists in database.";

    public ApplicationRequestNotFoundException(BigInteger id) {
        super(messagePart1 + id.toString() + messagePart2);
    }
}
