package com.nikita2304.tinkoff_test.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.math.BigInteger;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ContactNotFoundException extends Exception {

    private static final String message = "There is no contact in database, contact_id = ";

    public ContactNotFoundException(BigInteger id) {
        super(message + id.toString());
    }
}
