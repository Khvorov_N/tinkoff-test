package com.nikita2304.tinkoff_test.model;

import java.math.BigInteger;
import java.util.Date;

public class ApplicationRequest {

    private BigInteger applicationId;
    private Date dtCreated;
    private String productName;
    private BigInteger contactId;

    public ApplicationRequest() {
    }

    public ApplicationRequest(BigInteger applicationId, Date dtCreated, String product, BigInteger contactId) {
        this.applicationId = applicationId;
        this.dtCreated = dtCreated;
        this.productName = product;
        this.contactId = contactId;
    }

    public BigInteger getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(BigInteger applicationId) {
        this.applicationId = applicationId;
    }

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigInteger getContactId() {
        return contactId;
    }

    public void setContactId(BigInteger contactId) {
        this.contactId = contactId;
    }
}
