package com.nikita2304.tinkoff_test.model;

import java.math.BigInteger;

public class Contact {

    private BigInteger contactId;
    private String name;
    private String surname;

    public Contact() {
    }

    public Contact(BigInteger contactId, String name, String surname) {
        this.contactId = contactId;
        this.name = name;
        this.surname = surname;
    }

    public BigInteger getContactId() {
        return contactId;
    }

    public void setContactId(BigInteger contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
