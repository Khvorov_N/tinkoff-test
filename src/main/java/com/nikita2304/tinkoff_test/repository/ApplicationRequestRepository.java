package com.nikita2304.tinkoff_test.repository;

import com.nikita2304.tinkoff_test.exceptions.ApplicationRequestNotFoundException;
import com.nikita2304.tinkoff_test.model.ApplicationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ApplicationRequestRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String GET_ALL_REQUESTS_QUERY = "SELECT * FROM REQUESTS";
    private static final String GET_LATEST_REQUEST_BY_CONTACT_ID = "SELECT * FROM REQUESTS WHERE CONTACT_ID = ?" +
            " ORDER BY DT_CREATED DESC LIMIT 1";


    class RequestRowMapper implements RowMapper<ApplicationRequest> {

        @Override
        public ApplicationRequest mapRow(ResultSet resultSet, int i) throws SQLException {
            ApplicationRequest request = new ApplicationRequest();
            request.setApplicationId(BigInteger.valueOf(resultSet.getLong("application_id")));
            request.setDtCreated(resultSet.getDate("dt_created"));
            request.setProductName(resultSet.getString("product_name"));
            request.setContactId(BigInteger.valueOf(resultSet.getLong("contact_id")));
            return request;
        }
    }

    public List<ApplicationRequest> getAllRequests() {
        return jdbcTemplate.query(GET_ALL_REQUESTS_QUERY, new RequestRowMapper());
    }

    public ApplicationRequest getLatestRequestByCustomerId(BigInteger id) throws ApplicationRequestNotFoundException {
        List<ApplicationRequest> requests = jdbcTemplate.query(GET_LATEST_REQUEST_BY_CONTACT_ID,
                new Object[]{id}, new RequestRowMapper());
        if (requests.size() == 0) {
            throw new ApplicationRequestNotFoundException(id);
        }

        return requests.get(0);
    }

}
