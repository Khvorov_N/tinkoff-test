package com.nikita2304.tinkoff_test.repository;

import com.nikita2304.tinkoff_test.exceptions.ContactNotFoundException;
import com.nikita2304.tinkoff_test.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ContactRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String GET_ALL_CONTACTS_QUERY = "SELECT * FROM CONTACTS";
    private static final String GET_CONTACT_BY_ID_QUERY = "SELECT * FROM CONTACTS WHERE CONTACT_ID = ?";

    class ContactRowMapper implements RowMapper<Contact> {

        @Override
        public Contact mapRow(ResultSet resultSet, int i) throws SQLException {
            Contact contact = new Contact();
            contact.setContactId(BigInteger.valueOf(resultSet.getLong("contact_id")));
            contact.setName(resultSet.getString("name"));
            contact.setSurname(resultSet.getString("surname"));
            return contact;
        }
    }

    public List<Contact> getAllContacts() {
        return jdbcTemplate.query(GET_ALL_CONTACTS_QUERY, new ContactRowMapper());
    }

    public Contact getContact(BigInteger contactId) throws ContactNotFoundException {
        List<Contact> contacts = jdbcTemplate.query(GET_CONTACT_BY_ID_QUERY,
                new Object[]{contactId}, new ContactRowMapper());
        if (contacts.size() == 0) {
            throw new ContactNotFoundException(contactId);
        }

        return contacts.get(0);
    }
}
