insert into CONTACTS(contact_Id, name, surname) values
 (1,'Mitch','Connor'),
 (2,'Nick','Rivera'),
 (3,'Stan', 'Smith'),
 (4,'Loser','Big');

insert into requests(application_id, dt_created, product_name, contact_id) values
(1,TO_DATE('2008/05/03', 'yyyy/mm/dd'),'A',1),
(2,TO_DATE('2012/06/04', 'yyyy/mm/dd'),'B',2),
(3,TO_DATE('2003/01/05', 'yyyy/mm/dd'),'A',3),
(4,TO_DATE('2004/12/06', 'yyyy/mm/dd'),'C',1),
(5,TO_DATE('2003/10/07', 'yyyy/mm/dd'),'D',2),
(6,TO_DATE('2005/08/08', 'yyyy/mm/dd'),'B',3),
(7,TO_DATE('2003/03/09', 'yyyy/mm/dd'),'K',1);

