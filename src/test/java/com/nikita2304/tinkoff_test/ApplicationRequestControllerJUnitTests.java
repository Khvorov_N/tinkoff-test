package com.nikita2304.tinkoff_test;


import com.nikita2304.tinkoff_test.controller.RequestController;
import com.nikita2304.tinkoff_test.model.ApplicationRequest;
import com.nikita2304.tinkoff_test.repository.ApplicationRequestRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.BasicJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@WebMvcTest(RequestController.class)
public class ApplicationRequestControllerJUnitTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ApplicationRequestRepository requestRepository;

    @Test
    public void testGetAllRequests() throws Exception {
        Date date = new Date();
        when(requestRepository.getAllRequests()).thenReturn(Collections.singletonList(new ApplicationRequest(BigInteger.ONE, date, "ALL", BigInteger.ONE)));
        MvcResult result = mockMvc.perform(get("/requests")).andReturn();
        JsonParser parser = new BasicJsonParser();
        Map params = (Map) parser.parseList(result.getResponse().getContentAsString()).get(0);
        Assert.assertEquals(BigInteger.valueOf((Long) params.get("applicationId")), BigInteger.ONE);
        Assert.assertEquals(params.get("productName"), "ALL");
        Assert.assertEquals(BigInteger.valueOf((Long) params.get("contactId")), BigInteger.ONE);
    }

    @Test
    public void testGetRequest() throws Exception {
        Date date = new Date();
        when(requestRepository.getLatestRequestByCustomerId(BigInteger.ONE)).thenReturn(new ApplicationRequest(BigInteger.ONE, date, "Single", BigInteger.ONE));
        MvcResult result = mockMvc.perform(get("/requests/requests-by-contact/1")).andReturn();
        JsonParser parser = new BasicJsonParser();
        Map params = parser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals(BigInteger.valueOf((Long) params.get("applicationId")), BigInteger.ONE);
        Assert.assertEquals(params.get("productName"), "Single");
        Assert.assertEquals(BigInteger.valueOf((Long) params.get("contactId")), BigInteger.ONE);
    }
}
