package com.nikita2304.tinkoff_test;

import com.nikita2304.tinkoff_test.controller.ContactController;
import com.nikita2304.tinkoff_test.model.Contact;
import com.nikita2304.tinkoff_test.repository.ContactRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigInteger;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


@RunWith(SpringRunner.class)
@WebMvcTest(ContactController.class)
public class ContactControllerJUnitTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ContactRepository contactRepository;

    @Test
    public void testGetAllContacts() throws Exception {
        when(contactRepository.getAllContacts()).thenReturn(Collections.singletonList(new Contact(BigInteger.ONE, "All", "Contacts")));
        MvcResult result = mockMvc.perform(get("/contacts")).andReturn();
        String actualResult = "[{\"contactId\":1,\"name\":\"All\",\"surname\":\"Contacts\"}]";
        Assert.assertEquals(result.getResponse().getContentAsString(), actualResult);
    }

    @Test
    public void testGetContact() throws Exception {
        when(contactRepository.getContact(BigInteger.valueOf(1))).thenReturn(new Contact(BigInteger.ONE, "John", "Doe"));
        MvcResult result = mockMvc.perform(get("/contacts/1")).andReturn();
        String actualResult = "{\"contactId\":1,\"name\":\"John\",\"surname\":\"Doe\"}";
        Assert.assertEquals(result.getResponse().getContentAsString(), actualResult);
    }
}
