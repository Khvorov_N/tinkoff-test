package com.nikita2304.tinkoff_test;


import com.nikita2304.tinkoff_test.exceptions.ContactNotFoundException;
import com.nikita2304.tinkoff_test.exceptions.ApplicationRequestNotFoundException;
import com.nikita2304.tinkoff_test.model.ApplicationRequest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.BasicJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigInteger;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MySpringBoot2ApplicationTests {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() {
        for (String beanName : context.getBeanDefinitionNames()) {
            System.out.println("bean: " + beanName + " loaded.");
        }
    }

    @Test
    public void testGeneralMessage() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/").accept(
                MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        String expected = "This is test application!";
        Assert.assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
        Assert.assertEquals(result.getResponse().getContentAsString(), expected);
    }

    @Test
    public void testAllContacts() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/contacts").accept(
                MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        JsonParser parser = new BasicJsonParser();
        Collection objects = parser.parseList(result.getResponse().getContentAsString());
        Assert.assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
        Assert.assertEquals(objects.size(), 4);

    }


    @Test
    public void testAllRequests() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/requests").accept(
                MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        JsonParser parser = new BasicJsonParser();
        Collection objects = parser.parseList(result.getResponse().getContentAsString());
        Assert.assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
        Assert.assertEquals(objects.size(), 7);
    }


    @Test
    public void testRequestByCustomerId() throws Exception {
        ApplicationRequest testRequest = new ApplicationRequest(BigInteger.valueOf(1), new Date("2008/05/03"), "A", BigInteger.valueOf(1));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/requests/requests-by-contact/1").accept(
                MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        JsonParser parser = new BasicJsonParser();
        System.out.println(result.getResponse().getContentAsString());
        Map params = parser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
        Assert.assertEquals(BigInteger.valueOf((Long) params.get("applicationId")), testRequest.getApplicationId());
        Assert.assertEquals(new Date(params.get("dtCreated").toString().replaceAll("-", "/")), testRequest.getDtCreated());
        Assert.assertEquals(params.get("productName"), testRequest.getProductName());
    }


    @Test
    public void testExceptionOnMissingRequest() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/requests/requests-by-contact/4").accept(
                MediaType.APPLICATION_JSON);
        String message = "There is no requests in database for contact_id = 4. Check if customer exists in database.";
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        JsonParser parser = new BasicJsonParser();
        Map params = parser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals(result.getResponse().getStatus(), HttpStatus.NOT_FOUND.value());
        Assert.assertEquals(result.getResolvedException().getClass(), ApplicationRequestNotFoundException.class);
        Assert.assertEquals(params.get("message"), message);

    }

    @Test
    public void testExceptionOnMissingContact() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/contacts/5").accept(
                MediaType.APPLICATION_JSON);
        String message = "There is no contact in database, contact_id = 5";
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        JsonParser parser = new BasicJsonParser();
        Map params = parser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals(result.getResponse().getStatus(), HttpStatus.NOT_FOUND.value());
        Assert.assertEquals(result.getResolvedException().getClass(), ContactNotFoundException.class);
        Assert.assertEquals(params.get("message"), message);
    }
}
